/*
* Cabinet Latch Rail Guide (c) by Andrew A. Tasso
* 
* Cabinet Latch Rail Guide is licensed under a Creative Commons 
* Attribution-NonCommercial-ShareAlike 4.0 International License.
* 
* You should have received a copy of the license along with this
* work. If not, see <http://creativecommons.org/licenses/by-nc-sa/4.0/>.
*/

use <./lib/dotSCAD/multi_line_text.scad>
use <./lib/dotSCAD/rounded_square.scad>

// keep the number of fragments low while in preview to improve performance
$fn = $preview ? 20 : 100;
eps = 0.001;

cabinet_latch_rail_guide();

module cabinet_latch_rail_guide() {

    difference() {

        union() {

            linear_extrude(height = 1)
                rounded_square(size = [25.6, 13], corner_r = 4);

            translate(v = [2.8, 1.5, 0]) 
                linear_extrude(height = 12.3)
                rounded_square(size = [20, 10], corner_r = 1.6);

        }

        translate(v = [5.425,4,-eps]) 
            cube(size = [14.75,5,12.3 + 2 * eps]);

        translate(v = [12.8, 2, 7.5]) 
            rotate(a = [90,180,0])
            linear_extrude(height = 1) 
            multi_line_text(["TASSO", ".DEV"], line_spacing = 4, size = 3.5, valign = "center", halign = "center", font = "Liberation Sans:style=Bold");

    }

}
